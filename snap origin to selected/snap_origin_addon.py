bl_info = {
    "name": "Snap Origin to selected",
    "author": "jgargallo",
    "version": (0, 5),
    "blender": (2, 92, 0),
    #"location": "View3D > Mesh > Snap > Snap Origin to selected",
    "description": "snaps the origin of the object to a selection",
    "category": "Mesh",
}

import bpy
from bpy.props import (BoolProperty)
class MESH_OT_snapOrigin(bpy.types.Operator):
    """It has to begin with a selected element in edit mode"""
    bl_idname = "mesh.snap_origin_to_selected"
    bl_label = "Snap Origin to selected"
    bl_options = {'REGISTER', 'UNDO'}
    
    only_active: bpy.props.BoolProperty(
        name="set only active",
        description="set only the origin of the active object",
        default=False,
    )
    

    @classmethod
    def poll(cls, context):
        return context.area.type == 'VIEW_3D'
    

    def execute(self, context):
        
        get_3dCursor = (bpy.context.scene.cursor.location[0],bpy.context.scene.cursor.location[1],bpy.context.scene.cursor.location[2])

        bpy.ops.view3d.snap_cursor_to_selected()
        bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        if self.only_active:
            # override dictionary to only provide active object as selected objects (correct context for the operator)
            c = {}
            c['active_object'] = None
            c['selected_editable_objects'] = [bpy.context.object]
            print(c)
            bpy.ops.object.origin_set(c,type='ORIGIN_CURSOR')
        else:
           bpy.ops.object.origin_set(type='ORIGIN_CURSOR', center='MEDIAN')
        bpy.context.scene.cursor.location[0] = get_3dCursor[0]
        bpy.context.scene.cursor.location[1] = get_3dCursor[1]
        bpy.context.scene.cursor.location[2] = get_3dCursor[2]
        return {'FINISHED'}

# Registration

def set_origin_button(self, context):
    self.layout.operator(
        MESH_OT_snapOrigin.bl_idname,
        text="snap origin to selected",
        icon='PIVOT_CURSOR')
        
        
def register():
    bpy.utils.register_class(MESH_OT_snapOrigin)
    bpy.types.VIEW3D_MT_edit_mesh.append(set_origin_button)

def unregister():
    bpy.utils.unregister_class(MESH_OT_snapOrigin)
    bpy.types.VIEW3D_MT_edit_mesh.remove(set_origin_button)